import Data.List

{-
This is the hand in for assignment 3, String Alignment, in the course EDAN40.
Author: Joakim Brorsson
-}

{-
Assignment 1: By using a positive score for aligned characters and zero for spaces
and mismatches, the only thing importatnt will be alignment of characters. By doing 
this and after that measuring the length of the longest sequence, String Alignment 
can be used to solve for the MCS
-}

-- Alignment constants
scoreMatch = 0
scoreMismatch = -1
scoreSpace = -1

-- Helper functions for recursion in similariryScore 
ySpace (x:xs) (y:ys)	= score x '-' + similarityScore xs (y:ys)
xSpace (x:xs) (y:ys)	= score '-' y + similarityScore (x:xs) ys
same   (x:xs) (y:ys)	= score x y + similarityScore xs ys

-- 2) A:
slowSimilarityScore :: String -> String -> Int
slowSimilarityScore [] [] = 0
slowSimilarityScore (x:xs) [] = similarityScore xs [] + score x '-'
slowSimilarityScore [] (y:ys) = similarityScore [] ys + score '-' y
slowSimilarityScore (x:xs) (y:ys) = max (same (x:xs) (y:ys)) (max (xSpace (x:xs) (y:ys)) (ySpace (x:xs) (y:ys)))

-- Helper function for scoring alignments
score :: Char -> Char -> Int
score _ '-' = scoreSpace
score '-' _ = scoreSpace
score x y
	| x == y = scoreMatch
	| otherwise = scoreMismatch

{-
2) B:
This function takes a list of pairs of strings. For every pair, it attaches the
letter h1 in the beginning of the first string in the pair and h2 to the second.
-}
attachHeads :: a -> a -> [([a],[a])] -> [([a],[a])] 
attachHeads h1 h2 aList = [(h1:xs,h2:ys) | (xs,ys) <- aList]

-- Same as attachHead but it attches the letters in the end of the strings 
attachToHeads :: a -> a -> [([a],[a])] -> [([a],[a])] 
attachToHeads h1 h2 aList = [(xs++[h1],ys++[h2]) | (xs,ys) <- aList]

{-
2) C:
This function takes a list and return a list containing the set of greatest elements from the 
input list, according to the comparison-function valueFcn
-}
maximaBy :: Ord b => (a -> b) -> [a] -> [a]
maximaBy valueFcn xs = [ x | x <- xs, (maximum (map valueFcn xs)) == valueFcn x]

type AlignmentType = (String,String)

-- 2) D:
slowOptAlignments :: String -> String -> [AlignmentType]
slowOptAlignments [] [] 		= [([],[])]
slowOptAlignments (x:xs) [] 	= attachHeads x '-' $ optAlignments xs []
slowOptAlignments [] (y:ys) 	= attachHeads '-' y $ optAlignments [] ys
slowOptAlignments (x:xs) (y:ys) = maximaBy (\(a, b) -> sum (zipWith score a b)) $ concat [attachHeads x y (optAlignments xs ys), attachHeads '-' y (optAlignments (x:xs) ys), attachHeads x '-' (optAlignments xs (y:ys))]

-- 2) E:
outputOptAlignments :: String -> String -> IO ()
outputOptAlignments s1 s2 = do
	putStrLn $ "There are " ++ (show (length ali :: Int)) ++ " optimal alignments:"
	mapM_ (\(a, b) -> putStrLn ("\n" ++ a ++ "\n" ++ b)) ali -- ++ "\n" ++ snd ali 
		where ali = optAlignments s1 s2

-- 3) A:
similarityScore :: String -> String -> Int
similarityScore xs ys = simScore (length xs) (length ys)
	where
		simScore i j = simTable!!i!!j
		simTable = [[simEntry i j | j<-[0..]] | i<-[0..]]

		simEntry :: Int -> Int -> Int
		simEntry 0 0 = 0
		simEntry i 0 = scoreSpace + simEntry (i-1) 0
		simEntry 0 j = scoreSpace + simEntry 0 (j-1)
		simEntry i j = max (score x y + simScore (i-1) (j-1)) (max (score x '-' + simScore (i-1) (j)) (score '-' y + simScore (i) (j-1)) )
			where
         		x = xs!!(i-1)
         		y = ys!!(j-1)

-- 3) B:
optAlignments :: String -> String -> [AlignmentType]
optAlignments xs ys = snd (optAli (length xs) (length ys) )
	where 
		aliEntry{-, aliTable, optAli -}:: Int -> Int -> (Int, [AlignmentType])
		optAli i j = aliTable!!i!!j
		aliTable = [[aliEntry i j | j<-[0..] ] | i<-[0..]]
		aliEntry 0 0 = (0, [([],[])])
		aliEntry i 0 = (scoreSpace + (fst $ optAli (i-1) 0), attachToHeads (xs!!(i-1)) '-' $  snd $ optAli (i-1) 0)
		aliEntry 0 j = (scoreSpace + (fst $ optAli 0 (j-1)), attachToHeads '-' (ys!!(j-1)) $ snd $ optAli 0 (j-1)) 
		aliEntry i j = (fst (head gb), concat (map snd gb))
		    where
         		x         = xs!!(i-1)
         		y         = ys!!(j-1)
         		cd l1 l2 i1 i2 = (score l1 l2 + fst (optAli i1 i2), attachToHeads l1 l2 ( snd ( optAli i1 i2)))
         		gb        = maximaBy fst [cd x y (i-1) (j-1), cd x '-' (i-1) j, cd '-' y i (j-1)]

